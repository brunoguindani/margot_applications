#ifndef STEREOMATCH_PROXIMITY_FUNCTIONS_HDR
#define STEREOMATCH_PROXIMITY_FUNCTIONS_HDR

#include <cstdint>

#include <opencv2/core/core.hpp>

namespace stereomatch
{

	inline float compute_closeness( const cv::Mat& image, const unsigned char threshold )
  {
    // get the geomtry of the output image
    const auto image_size = image.size();

    // get some constants out of the problem
    const int_fast32_t height = image_size.height;
    const int_fast32_t width = image_size.width;

    // declare the variable that holds the number of pixels above threshold
    int_fast32_t close_counter = 0;

    // iterate over the image to count the close objects
    for ( int_fast32_t y = 0; y < height; ++y )
    {
      for ( int_fast32_t x = 0; x < width; ++x )
      {
        // check if the pixel is above threshold
        if (image.at<unsigned char>(y, x) > threshold )
        {
          ++close_counter;
        }
      }
    }

    // compute the percentage
    return (static_cast<float>(close_counter) / static_cast<float>(height * width)) * 100.0f;
  }

}

#endif // STEREOMATCH_PROXIMITY_FUNCTIONS_HDR
