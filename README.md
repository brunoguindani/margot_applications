# Introduction

This repository contains a list of applications integrated with the mARGOt autotuning framework.

Available applications:
  - swaptions from the PARSEC benchmark suite ( C++/OpenMP ) [Original version][1]

# File organization

This repository uses a two-level hierarchy organization.

## The repository root

The repository root is organized as follows:

><root>
>├── beautify.sh
>├── bootstrap.sh
>├── dse_conf
>│   ├── example
>│   └── <hostname>
>└── <applications>

The bash script _bootstrap.sh_ downloads and compiles the mARGOt autotuning framework.
Indeed, the framework consider the application knowledge as an input file for the generation of the high-level interface. However, we provides a simple Design Space Exploration tool, that performs a full-factorial Design of Experiment.
To this end, the folder _dse_conf/example_ contains the configuration file that describes the DoE for each application. Since the actual values of the software-knobs also depends on the platform, the bootstrap script generates a copy of the folder using the hostname.
In this way, it is possible to change the files in _dse_conf/<hostname>_ to change the DoE.

The script _beautify-sh_ is an utility file which apply a coherent code style on the C++ source files, using the tool _astyle_.


## Application organization

Each application is already integrated with the autotuner and the files are organized as follows:

><application>
>├── build.sh
>├── clean.sh
>├── CMakeLists.txt
>├── config
>│   └── margot.xml
>├── include
>│   └── <headers>
>├── perform_dse.sh
>└── src
>    └── <sources>

In particular, the source files of the application are contained in the _include_ and _src_ folders. The mARGOt configuration file is _config/margot.xml_. You are able to change the latter if you want to modify the adaptation layer.

Moreover, there are three bash script to automatize the building process:

1. _build.sh_
...Generates the executable of the application. In particular, it generates the high level interface from the mARGOt configuration file, if compiles it and then it compiles the actual application.
...If it is available the application knowledge in _dse/oplist.<application_name>.<block_name>.conf_, it generates the high level interface with the Operating Point List.

2. _perform_dse.sh_
...The mARGOt autotuning framework considers the application knowledge as an input file. However, it is possible to use this script to perform a full-factorial DSE. Since the DSE is based on Makefiles, it is possible to pause and resume the DSE.

3. _clean.sh_
...Removes all the generated files, except the ones of the Design Space Exploration.



# How to build an application

Let's assume that you want to play with the ``swaptions`` application, you have to perform the following actions:

1. Clone and bootstrap this repository:
```shell
git clone https://gitlab.com/brunoguindani/margot_applications
cd margot_applications
./bootstrap.sh
```

2. Obtain the application knowledge (it may take a while)
```shell
cd swaptions
./perform_dse.sh
```

3. Compile the application
```shell
./build.sh
```

4. Execute the application
```shell
cd bin
./swaptions.x --help
```



[1]: http://parsec.cs.princeton.edu
