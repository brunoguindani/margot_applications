#/bin/bash

# per application definitions
APPLICATION_NAME=swaptions
BLOCK_NAME=pricing


#### DO NOT MODIFY UNDER THIS MARK ####


# check if we are inside a proper git repository
if git rev-parse --git-dir > /dev/null 2>&1; then :
  PROJECT_ROOT=$(git rev-parse --show-toplevel)
else :
  echo "Error: you must execute this script in the benchmark repository"
  exit -1
fi

# compute the paths
PROJECT_ROOT=$(git rev-parse --show-toplevel)
APPLICATION_ROOT=$PROJECT_ROOT/$APPLICATION_NAME
WORKING_DIRECTORY=$PWD


# check if we have to bootstrap the repository
[ -f "${PROJECT_ROOT}/autotuner/margot_heel/margot_heel_if/cmake/FindMARGOT.cmake" ] || sh $PROJECT_ROOT/bootstrap.sh

# start with a fresh high level interface
rm -r -f $APPLICATION_ROOT/high_level_interface
cp -r $PROJECT_ROOT/autotuner/margot_heel/margot_heel_if $APPLICATION_ROOT/high_level_interface || exit -1
rm $APPLICATION_ROOT/high_level_interface/config/*.conf || exit -1


# place all the margot configuration files in the right place
cp $APPLICATION_ROOT/config/margot.xml $APPLICATION_ROOT/high_level_interface/config/margot.conf || exit -1
if [ -f "${APPLICATION_ROOT}/dse/oplist.${APPLICATION_NAME}.${BLOCK_NAME}.conf" ];
then
  echo "Found the application knowledge"
  cp ${APPLICATION_ROOT}/dse/oplist.${APPLICATION_NAME}.${BLOCK_NAME}.conf $APPLICATION_ROOT/high_level_interface/config/ || exit -1
fi

# compile the high level interface
mkdir -p $APPLICATION_ROOT/high_level_interface/build || exit -1
cd $APPLICATION_ROOT/high_level_interface/build
cmake -DCMAKE_BUILD_TYPE=Release -DMARGOT_CONF_FILE=margot.conf .. || exit -1
make || exit -1


# compile the application itself
mkdir -p $APPLICATION_ROOT/build || exit -1
cd $APPLICATION_ROOT/build
cmake -DCMAKE_INSTALL_PREFIX:PATH=$APPLICATION_ROOT -DCMAKE_BUILD_TYPE=Release .. || exit -1
make || exit -1
make install || exit -1

# restore the working directory
cd $WORKING_DIRECTORY
