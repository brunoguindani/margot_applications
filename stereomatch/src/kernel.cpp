#include <stdexcept>

#include <kernel.hpp>

namespace stereomatch
{

  kernel::kernel( void )
    : image_width(0), image_height(0), grayscale(3), max_hypo_value(90), hypo_step(1), max_arm_length(16),
      color_threshold(60), matchcost_limit(100), num_threads(1)
  {}


  cv::Mat kernel::operator()( const cv::Mat& left_image, const cv::Mat& right_image, const software_knobs& knobs )
  {
    // set the software knobs for the elaboration
    max_hypo_value = knobs.max_hypo_value;
    hypo_step = knobs.hypo_step;
    max_arm_length = knobs.max_arm_length;
    color_threshold = knobs.color_threshold;
    matchcost_limit = knobs.matchcost_limit;
    num_threads = knobs.num_threads;

    // we consider the left image to query information about the content
    const auto geometry = left_image.size();

    // make sure that it is the same for the right image
    if (right_image.size() != geometry)
    {
      throw std::runtime_error("Error: the left and right image shall have the same geometry");
    }

    // check if we need to resize the internal support structures
    if ((geometry.height != image_height) || (geometry.width != image_width))
    {
      // resize the support regions
      std::size_t number_of_pixel = static_cast<std::size_t>(geometry.height) * static_cast<std::size_t>(geometry.width);
      crosses_lx.resize(number_of_pixel, {});
      crosses_rx.resize(number_of_pixel, {});
      raw_matchcost.resize(number_of_pixel, {});
      matchcost.resize(number_of_pixel, {});
      anchorreg.resize(number_of_pixel, {});
      fmatchcost.resize(number_of_pixel, {});
      fanchorreg.resize(number_of_pixel, {});
      dpr_votes.resize(number_of_pixel, {});
      dpr_block.resize(number_of_pixel, {});

      // update the variables
      image_height = geometry.height;
      image_width = geometry.width;
    }

    // declare the output image
    cv::Mat output_image(geometry, CV_8UC1, cv::Scalar(255));

    // get naked pointers out of the OpenCV structure
    const rgb_t* left = reinterpret_cast<const rgb_t*>(left_image.data);
    const rgb_t* right = reinterpret_cast<const rgb_t*>(right_image.data);
    unsigned char* output = reinterpret_cast<unsigned char*>(output_image.data);

    // this is the actual kernel of the application
    #pragma omp parallel num_threads(num_threads)
    {
      // Build support regions
      winBuild(crosses_lx, left);
      winBuild(crosses_rx, right);

      // Evaluate disparity hypotheses
      for (int d = 0; d <= max_hypo_value; d += hypo_step)
      {
        raw_horizontal_integral(left, right, d);
        horizontal_integral(d);
        vertical_integral(d);
        crossregion_integral(d);
      }

      // Refinement filter
      refinement(output);
    }

    // return the computed disparity map
    return output_image;
  }




  //////////////////////////////////////////////////////
  /// STEREOMATCH KERNEL FUNCTIONS
  //////////////////////////////////////////////////////



  bool similar(rgb_t p, rgb_t q, int color_threshold)
  {
    if (abs( (int)p.B - (int)q.B ) > color_threshold ||
        abs( (int)p.G - (int)q.G ) > color_threshold ||
        abs( (int)p.R - (int)q.R ) > color_threshold)
    {
      return false;
    }

    return true;
  }


  void kernel::winBuild(std::vector< cross_t >& crosses, const rgb_t* bitmap)
  {
    #pragma omp for

    for (int py = 0; py < image_height; py++)
    {
      for (int px = 0; px < image_width; px++)
      {
        int c;
        const size_t offset = py * image_width + px;

        rgb_t p = bitmap[offset];

        // Move left
        if (px > 0)
        {
          c = 1;

          while (px - c >= 0 && similar(p, bitmap[py * image_width + px - c], color_threshold) && c <= max_arm_length)
          {
            c++;
          }

          if (c > 1)
          {
            c--;
          }
        }
        else
        {
          c = 0;
        }

        // Set arm length
        crosses[offset].l = c;

        // Move right
        if (px < image_width - 1)
        {
          c = 1;

          while (px + c < image_width && similar(p, bitmap[py * image_width + px + c], color_threshold) && c <= max_arm_length)
          {
            c++;
          }

          if (c > 1)
          {
            c--;
          }
        }
        else
        {
          c = 0;
        }

        // Set arm length
        crosses[offset].r = c;

        // Move up
        if (py > 0)
        {
          c = 1;

          while (py - c >= 0 && similar(p, bitmap[(py - c)*image_width + px], color_threshold) && c <= max_arm_length)
          {
            c++;
          }

          if (c > 1)
          {
            c--;
          }
        }
        else
        {
          c = 0;
        }

        // Set arm length
        crosses[offset].u = c;

        // Move down
        if (py < image_height - 1)
        {
          c = 1;

          while (py + c < image_height && similar(p, bitmap[(py + c)*image_width + px], color_threshold) && c <= max_arm_length)
          {
            c++;
          }

          if (c > 1)
          {
            c--;
          }
        }
        else
        {
          c = 0;
        }

        // Set arm length
        crosses[offset].d = c;
      }
    }
  }

  int pixel_aggr_cost(rgb_t p, rgb_t q, int matchcost_trunc)
  {
    int cost = abs((int)p.B - (int)q.B) + abs((int)p.G - (int)q.G) + abs((int)p.R - (int)q.R);

    if (cost > matchcost_trunc)
    {
      return matchcost_trunc;
    }
    else
    {
      return cost;
    }
  }

  void kernel::raw_horizontal_integral(const rgb_t* bitmapLx, const rgb_t* bitmapRx, int disp_cur)
  {
    const int xlimit = image_width - 1 - disp_cur;

    #pragma omp for

    for (int y = 0; y < image_height; y++)
    {
      for (int x = 0; x < image_width; x++)
      {
        const size_t offset = y * image_width + x;

        if (x > xlimit)
        {
          raw_matchcost[offset] = raw_matchcost[offset - 1] + matchcost_limit;
        }
        else
        {
          int cost = pixel_aggr_cost(bitmapRx[offset], bitmapLx[offset + disp_cur], matchcost_limit);

          if (x == 0)
          {
            raw_matchcost[offset] = cost;
          }
          else
          {
            raw_matchcost[offset] = raw_matchcost[offset - 1] + cost;
          }
        }
      }
    }
  }

  void get_combined_cross(cross_t a, cross_t b, cross_t* res)
  {
    if (a.d < b.d)
    {
      res->d = a.d;
    }
    else
    {
      res->d = b.d;
    }

    if (a.l < b.l)
    {
      res->l = a.l;
    }
    else
    {
      res->l = b.l;
    }

    if (a.r < b.r)
    {
      res->r = a.r;
    }
    else
    {
      res->r = b.r;
    }

    if (a.u < b.u)
    {
      res->u = a.u;
    }
    else
    {
      res->u = b.u;
    }
  }

  void kernel::horizontal_integral(int disp_cur)
  {
    const int xlimit = image_width - 1 - disp_cur;

    #pragma omp for

    for (int y = 0; y < image_height; y++)
    {
      for (int x = 0; x < image_width; x++)
      {
        cross_t combined_cross;
        const size_t offset = y * image_width + x;

        if (x > xlimit)
        {
          combined_cross = crosses_rx[offset];
        }
        else
        {
          get_combined_cross(crosses_rx[offset], crosses_lx[offset + disp_cur], &combined_cross);
        }

        anchorreg[offset] = combined_cross.r + combined_cross.l + 1;

        if (x == 0 || x - combined_cross.l == 0)
        {
          matchcost[offset] = raw_matchcost[offset + combined_cross.r];
        }
        else
        {
          matchcost[offset] = raw_matchcost[offset + combined_cross.r] - raw_matchcost[offset - combined_cross.l - 1];
        }
      }
    }
  }

  void kernel::vertical_integral(int disp_cur)
  {
    #pragma omp for

    for (int x = 0; x < image_width; x++)
    {
      for (int y = 0; y < image_height; y++)
      {
        if (y > 0)
        {
          matchcost[y * image_width + x] += matchcost[(y - 1) * image_width + x];
          anchorreg[y * image_width + x] += anchorreg[(y - 1) * image_width + x];
        }
      }
    }
  }

  void kernel::crossregion_integral(int disp_cur)
  {
    const int xlimit = image_width - 1 - disp_cur;

    #pragma omp for

    for (int y = 0; y < image_height; y++)
    {
      for (int x = 0; x < image_width; x++)
      {
        cross_t combined_cross;
        const size_t offset = y * image_width + x;

        if (x > xlimit)
        {
          combined_cross = crosses_rx[offset];
        }
        else
        {
          get_combined_cross(crosses_rx[offset], crosses_lx[offset + disp_cur], &combined_cross);
        }

        if (y == 0 || y - combined_cross.u == 0)
        {
          fmatchcost[offset] = matchcost[(y + combined_cross.d) * image_width + x];
          fanchorreg[offset] = anchorreg[(y + combined_cross.d) * image_width + x];
        }
        else
        {
          fmatchcost[offset] = matchcost[(y + combined_cross.d) * image_width + x] - matchcost[(y - combined_cross.u - 1) * image_width + x];
          fanchorreg[offset] = anchorreg[(y + combined_cross.d) * image_width + x] - anchorreg[(y - combined_cross.u) * image_width + x];
        }

        int costonorm = fmatchcost[offset] / (fanchorreg[offset] + 1);

        if (disp_cur == 0 || costonorm < dpr_votes[offset].costo)
        {
          dpr_votes[offset].costo = costonorm;
          dpr_votes[offset].disparity = (unsigned char) (disp_cur & 0xFF);
        }
      }
    }
  }

  void kernel::refinement(unsigned char* disparity)
  {
    #pragma omp for

    for (int y = 0; y < image_height; y++)
    {
      for (int x = 0; x < image_width; x++)
      {
        size_t offset = y * image_width + x;

        for (int i = 0; i < 8; i++)
        {
          dpr_block[offset].bitnum[i] = 0;
        }

        for (int h = -crosses_rx[offset].l; h <= crosses_rx[offset].r; h++)
        {
          unsigned char disp = dpr_votes[offset + h].disparity;

          for (int i = 0; i < 8; i++)
          {
            dpr_block[offset].bitnum[i] += (disp >> i) & 0x1;
          }
        }

        dpr_block[offset].npixels = crosses_rx[offset].l + crosses_rx[offset].r + 1;
      }
    }

    #pragma omp for

    for (int y = 0; y < image_height; y++)
    {
      for (int x = 0; x < image_width; x++)
      {
        size_t offset = y * image_width + x;

        unsigned int npixels = 0;
        unsigned int bitmap[8] = {0, 0, 0, 0, 0, 0, 0, 0};

        for (int v = -crosses_rx[offset].u; v <= crosses_rx[offset].d; v++)
        {
          for (int i = 0; i < 8; i++)
          {
            bitmap[i] += dpr_block[(y + v) * image_width + x].bitnum[i];
          }

          npixels += dpr_block[(y + v) * image_width + x].npixels;
        }

        int disp = 0x00;

        for (int i = 0; i < 8; i++)
        {
          disp |= (npixels < (bitmap[i] << 1)) ? (0x1 << i) : 0x0;
        }

        int inty = disp * grayscale;

        if (inty > 255)
        {
          inty = 255;
        }

        disparity[offset] = (inty & 0xFF);
      }
    }
  }








}
