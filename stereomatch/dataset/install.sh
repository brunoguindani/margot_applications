#!/bin/bash


if [ ! -e "Tsukuba/left/frame_1.png" ]; then
	echo -e "\n\n\n*****************************************************************"
	echo "In order to install the Tsukuba dataset, please download the"
	echo "full version at the following address ( ~5GB or ~200MB ):"
	echo
	echo " http://www.cvlab.cs.tsukuba.ac.jp/dataset/tsukubastereo.php "
	echo
	echo "and copy the folders as stated next:"
	echo " illumination/daylight/*         -> dataset/Tsukuba/*"
	echo " groundtruth/disparity_maps/left -> dataset/Tsukuba/disparity_maps"
	echo -e "*****************************************************************"
else
	MEASURE=$(identify Tsukuba/left/frame_1.png | awk '{ print $3 }')
	if [ "$MEASURE" != "240x180" ]; then
		sh converter.sh
	fi
fi



exit 0
