#/bin/bash

# per application definitions
APPLICATION_NAME=swaptions
BLOCK_NAME=pricing


#### DO NOT MODIFY UNDER THIS MARK ####


# check if we are inside a proper git repository
if git rev-parse --git-dir > /dev/null 2>&1; then :
  PROJECT_ROOT=$(git rev-parse --show-toplevel)
else :
  echo "Error: you must execute this script in the benchmark repository"
  exit -1
fi

# compute the paths
PROJECT_ROOT=$(git rev-parse --show-toplevel)
APPLICATION_ROOT=$PROJECT_ROOT/$APPLICATION_NAME
WORKING_DIRECTORY=$PWD


# check if we already have a dse workspace to resume
if [ -d $APPLICATION_ROOT/dse ]; then
  echo "We already have a DSE workspace!"
  echo "Resuming the DSE!"

  # resume the DSE
  make -C $APPLICATION_ROOT/dse || echo "DSE interrupted! (you can resume it later)"

else
  echo "Creating a DSE workspace!"

  # compile the application in exploration mode
  /bin/bash $APPLICATION_ROOT/build.sh || exit -1

  # generate the workspace
  python3 $PROJECT_ROOT/autotuner/install/bin/margot_cli generate_dse --workspace $APPLICATION_ROOT/dse --executable $APPLICATION_ROOT/bin/${APPLICATION_NAME}.x $PROJECT_ROOT/dse_conf/$HOSTNAME/dse.${APPLICATION_NAME}.xml

  # resume the DSE
  make -C $APPLICATION_ROOT/dse || echo "DSE interrupted! (you can resume it later)"

fi

# finally copy the op list with the benchmark convention
cp $APPLICATION_ROOT/dse/oplist.conf $APPLICATION_ROOT/dse/oplist.$APPLICATION_NAME.$BLOCK_NAME.conf
