#!/bin/bash


# get the root path of the project
if git rev-parse --git-dir > /dev/null 2>&1; then :
	PROJECT_ROOT=$(git rev-parse --show-toplevel)
else :
	echo "Error: you must execute this script in the benchmark repository"
	exit -1
fi

# get the current working directory
CURRENT_CWD=$CWD

[ -d "${PROJECT_ROOT}/margot_project/core" ] || { git clone https://gitlab.com/margot_project/core.git ${PROJECT_ROOT}/autotuner && cd ${PROJECT_ROOT}/autotuner && git checkout 8321f82d209e9fa16afef499d45efce1833f4ae5 && cd ../..; }


# build the autotuner for the default behavior
mkdir -p $PROJECT_ROOT/autotuner/build || exit -1
cd $PROJECT_ROOT/autotuner/build
cmake -DCMAKE_INSTALL_PREFIX:PATH=$PROJECT_ROOT/autotuner/install -DCMAKE_BUILD_TYPE=Release .. || exit -1
make -j 4 || exit -1
make install -j 4 || exit -1
cd $PROJECT_ROOT


# copy the dse folder
mkdir -p ${PROJECT_ROOT}/dse_conf/${HOSTNAME}
cp ${PROJECT_ROOT}/dse_conf/example/* ${PROJECT_ROOT}/dse_conf/${HOSTNAME}


APPLICATION_NAME="stereomatch"
echo "Configuring ${APPLICATION_NAME}"
# copy the autotuner autocomplete file
touch ${PROJECT_ROOT}/autotuner/.clang_complete
cp ${PROJECT_ROOT}/autotuner/.clang_complete ${PROJECT_ROOT}/${APPLICATION_NAME} || exit -1
# append the path for the high-level interface
echo "-I${PROJECT_ROOT}/${APPLICATION_NAME}/high_level_interface" >> ${PROJECT_ROOT}/${APPLICATION_NAME}/.clang_complete


# go back in the right place
cd ${CURRENT_CWD}
