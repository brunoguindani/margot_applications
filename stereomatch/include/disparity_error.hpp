/**
 * Description: OpenMP implementation of the Stereo-Matching algorithm described
 *              in the article: Ke Zhang, Jiangbo Lu and Gauthier Lafruit,
 *              "Cross-Based Local Stereo Matching Using Orthogonal Integral
 *              Images", IEEE Transactions on Circuits and Systems for Video
 *              Technology, Vol. 19, no. 7, July 2009.
 *
 *     @author  Edoardo Paone, edoardo.paone@polimi.it
 *     @change  Davide Gadioli, davide.gadioli@polimi.it
 *
 *     Company  Politecnico di Milano
 *   Copyright  Copyright (c) 2014, Politecnico di Milano
 *
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#ifndef STEREOMATCH_DISPARITY_ERROR_HDR
#define STEREOMATCH_DISPARITY_ERROR_HDR

#include <cmath>
#include <stdexcept>
#include <limits>

#include <opencv2/core/core.hpp>


namespace stereomatch
{

  inline float compute_error( const cv::Mat& output_image, const cv::Mat& reference_image )
  {
    // get the geomtry of the output image
    const auto image_size = output_image.size();

    // check that the input and output image are equivalent
    if ((reference_image.size() != image_size) || (output_image.type() != reference_image.type()))
    {
      throw std::runtime_error("Error: unable to compute disparity error with different images");
    }

    // get some constants out of the problem
    const int height = image_size.height;
    const int width = image_size.width;

    // declare the variable that holds the difference
    unsigned int disparity = 0;

    // iterate over the image
    for ( int y = 0; y < height; ++y )
    {
      for ( int x = 0; x < width; ++x )
      {
        // get the value from the image
        const auto output_value = output_image.at<unsigned char>(y, x);
        const auto reference_value = reference_image.at<unsigned char>(y, x);

        // compute the absolute disparity error
        disparity += output_value > reference_value ? output_value - reference_value : reference_value - output_value;
      }
    }

    // perform a sort of normalization
    return (static_cast<float>(disparity) / static_cast<float>(height * width)) / static_cast<float>(std::numeric_limits<unsigned char>::max()) * 100.0f;
  }


}


#endif // STEREOMATCH_DISPARITY_ERROR_HDR
